package com.example.springbootapi.dataAccess;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springbootapi.dataobjects.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
