package com.example.springbootapi.dataAccess;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springbootapi.dataobjects.User;

@Repository
public interface UserUploadRepository extends CrudRepository<User, Long>{

}
