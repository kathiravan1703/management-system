package com.example.springbootapi.dataAccess;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.springbootapi.dataobjects.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByfirstName(String firstName);

	@Query(value = "SELECT * FROM user WHERE email_id = ?1", nativeQuery = true)
	User findByEmailAddress(String emailAddress);
	
	@Query(value = "SELECT * FROM user WHERE email_id = ?1", nativeQuery = true)
	User userLogin(String emailAddress);
}
