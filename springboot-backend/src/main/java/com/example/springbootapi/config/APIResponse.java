package com.example.springbootapi.config;

import org.json.JSONObject;

public class APIResponse {
	
	public APIResponse() {
		// TODO Auto-generated constructor stub
	}

	public JSONObject APIResponseObject(Boolean success, Object obj) {
		JSONObject json = new JSONObject();
		if (success) {
			json.put("success", true);
			json.put("payload", obj);
			return json;
		} else {
			json.put("success", false);
			json.put("payload", false);
			return json;
		}
	}
}
