package com.example.springbootapi.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.springbootapi.dataAccess.UserUploadRepository;
import com.example.springbootapi.dataobjects.User;

@Service
public class UserUploadService {

	@Autowired
	private UserUploadRepository userUploadRepository;

	@Value("${file.upload-dir}")
	String UPLOAD_Folder;

	@SuppressWarnings("resource")
	public void saveUserUploadData() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(UPLOAD_Folder + "user.csv"));
			String line = bufferedReader.readLine();
			while ((line = bufferedReader.readLine()) != null && !line.isEmpty()) {
				String[] data = line.split(",");
				User user = new User();
				user.setEmailId(data[2]);
				user.setFirstName(data[0]);
				user.setLastName(data[1]);
				userUploadRepository.save(user);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
