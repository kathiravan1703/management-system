package com.example.springbootapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springbootapi.dataAccess.UserRepository;
import com.example.springbootapi.dataobjects.Login;
import com.example.springbootapi.dataobjects.User;
import com.example.springbootapi.exception.ResourceNotFoundException;

import org.json.JSONException;
import org.json.JSONObject;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200/")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	private List<User> users;

	// User Login
	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody Login login) {
		User loggedInUser = userRepository.userLogin(login.getEmailID());
		try {
			if (loggedInUser != null && passwordEncoder.matches(login.getPassword(), passwordEncoder.encode(login.getPassword()))) {
				JSONObject obj = this.APIResponse(true, loggedInUser, "Logged in Successfully");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
			} else {
				JSONObject obj = this.APIResponse(true, null, "Login Failed");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			JSONObject obj = this.APIResponse(false, null, e.getMessage());
			return new ResponseEntity<String>(obj.toString(), HttpStatus.BAD_GATEWAY);
		}
	}

	// Get All Users
	@GetMapping("/users")
	public ResponseEntity<String> GetAllUsers() throws JSONException {
		this.users = userRepository.findAll();
		try {
			if (users.isEmpty()) {
				JSONObject obj = this.APIResponse(false, this.users, "Users Not Found");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.NOT_FOUND);
			} else {
				JSONObject obj = this.APIResponse(true, this.users, "");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
			}
		} catch (Exception ex) {
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	// Save user Rest API
	@PostMapping("/saveUsers")
	public ResponseEntity<String> saveUsers(@RequestBody User user) throws JSONException {
		User existingUser = userRepository.findByfirstName(user.getFirstName());
		User existingEmail = userRepository.findByEmailAddress(user.getEmailId());
		try {
			if (existingUser == null && existingEmail == null) {
				String password = user.getPassword();
				String encodedPswd = passwordEncoder.encode(password);
				user.setPassword(encodedPswd);
				User saveUser = userRepository.save(user);
				JSONObject obj = this.APIResponse(true, saveUser, "User Saved Successfully");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
			} else {
				JSONObject obj = this.APIResponse(false, null, "User FirstName and Email already exists!!!");
				return new ResponseEntity<String>(obj.toString(), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			JSONObject obj = this.APIResponse(false, null, e.getMessage());
			return new ResponseEntity<String>(obj.toString(), HttpStatus.BAD_GATEWAY);
		}
	}

	// Get User by ID
	@GetMapping("/users/{id}")
	public ResponseEntity<User> GetUSerById(@PathVariable Long id) {
		User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id + "Not found"));
		return ResponseEntity.ok(user);
	}

	public JSONObject APIResponse(Boolean success, Object obj, String message) {
		JSONObject json = new JSONObject();
		if (success) {
			json.put("success", true);
			json.put("payload", obj);
			json.put("message", message);
			return json;
		} else {
			json.put("success", false);
			json.put("payload", false);
			json.put("message", message);
			return json;
		}
	}

	public JSONObject APIResponse(Boolean success, List<?> obj, String message) {
		JSONObject json = new JSONObject();
		if (success) {
			json.put("success", true);
			json.put("payload", obj);
			json.put("message", message);
			return json;
		} else {
			json.put("success", false);
			json.put("payload", false);
			json.put("message", message);
			return json;
		}
	}

}
