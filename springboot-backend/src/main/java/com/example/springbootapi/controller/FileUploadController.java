package com.example.springbootapi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.springbootapi.service.UserUploadService;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class FileUploadController {

	@Value("${file.upload-dir}")
	String UPLOAD_Folder;
	
	@Autowired
	private UserUploadService userUploadService;

	@PostMapping("/userUpload")
	public ResponseEntity<Object> UserCSVUpload(@RequestParam("File") MultipartFile multipartFile) throws IOException {
		File userFile = new File(UPLOAD_Folder + multipartFile.getOriginalFilename());
		if (!userFile.exists()) {
			userFile.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(userFile);
			fileOutputStream.write(multipartFile.getBytes());
			fileOutputStream.close();
			return new ResponseEntity<>("File Uploaded Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("File Uploaded Failed, Something! Went Wrong Please Check System Admin", HttpStatus.BAD_GATEWAY);
		}
		
	}
	
	@PostMapping("/readCSV")
	public void ReadCSV() {
		userUploadService.saveUserUploadData();
	}

}
