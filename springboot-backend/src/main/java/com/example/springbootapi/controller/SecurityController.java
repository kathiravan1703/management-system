package com.example.springbootapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class SecurityController {
	
	@GetMapping("/getWelcome")
	public String greeting() {
		return "Spring Boot Security";
	}
	
	@GetMapping("/process")
	public String process() {
		return "Spring Boot Security Processing.....";
	}
	
	
}
